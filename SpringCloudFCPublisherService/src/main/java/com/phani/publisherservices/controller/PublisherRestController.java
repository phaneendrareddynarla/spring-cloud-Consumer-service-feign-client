package com.phani.publisherservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.phani.publisherservices.consumer.BookRestConsumer;
import com.phani.publisherservices.model.Book;

@RestController
@RequestMapping("/publisher")
public class PublisherRestController {
	
	@Autowired
	private BookRestConsumer bookRestConsumer;
	
	@GetMapping("/view")
	public String getAllBooks() {
		ResponseEntity<List<Book>> resp = bookRestConsumer.getAllBooks();
		return "From Publisher "+ resp.getBody();
	}
	
	@PostMapping("/save")
	public String createOnePublisherBook(@RequestBody Book book) {
		ResponseEntity<String> resp = bookRestConsumer.createNewBook(book);
		return "From PUBLISHER SAVE BOOK "+resp.getBody();
	}
	
	@DeleteMapping("/del/{id}")
	public String removeOnePublisherBook(@PathVariable Integer id) {
	ResponseEntity<String> resp = bookRestConsumer.deleteBook(id);
		return "FROM PUBLISHER DELETE BOOK=>" +resp.getBody();
	}

}
